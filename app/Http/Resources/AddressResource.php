<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Models\Address;

class AddressResource extends JsonResource
{
    /**
     * AddressResource constructor.
     * @param Address $address
     */
    public function __construct(Address $address)
    {
        $this->resource = $address;
    }

    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id' => $this->resource->id,
            'region_id' => $this->resource->region_id,
            'longitude' => $this->resource->longitude,
            'latitude' => $this->resource->latitude,
            'address' => $this->resource->address,
            'place_id' => $this->resource->place_id
        ];
    }
}

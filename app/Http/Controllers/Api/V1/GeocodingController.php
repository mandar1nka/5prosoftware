<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\AddressRequest;
use App\Http\Resources\AddressResource;
use App\Models\Address;
use App\Models\Region;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Throwable;

class GeocodingController extends Controller
{
    /**
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        try {
            $addresses = Address::all();

            $resource = AddressResource::collection($addresses);

            return response()->json($resource);

        } catch (Throwable $throwable) {
            Log::debug('Error message get all addresses from DB: ' . $throwable->getMessage());
        }

        return response()->json(['error' => 'Something was gone wrong.'], 400);
    }

    /**
     * @param AddressRequest $request
     * @return JsonResponse
     */
    public function show(AddressRequest $request): JsonResponse
    {
        try {
            $region = null;

            $response = Http::get('https://maps.googleapis.com/maps/api/geocode/json?latlng='
                . $request->get('longitude') . ','
                . $request->get('latitude') . '&key='
                . env('TOKEN_GEOCODING_API'));

            $data = json_decode($response->body());

            if (array_key_exists(5, $data->results[0]->address_components)) {
                /** @var Region $region */
                $region = Region::where('region', $data->results[0]->address_components[5]->long_name ?? null)->first();

                if (!$region) {
                    /** @var Region $region */
                    $region = Region::create([
                        'region' => $data->results[0]->address_components[5]->long_name ?? null,
                        'city' => $data->results[0]->address_components[3]->long_name
                    ]);
                }
            }

            /** @var Address $address */
            $address = Address::where('place_id', $data->results[0]->place_id)->first();

            if (!$address) {
                if ($region) {
                    /** @var Address $address */
                    $address = $region->addresses()->create([
                        'longitude' => $request->get('longitude'),
                        'latitude' => $request->get('latitude'),
                        'address' => $data->results[0]->formatted_address,
                        'place_id' => $data->results[0]->place_id
                    ]);
                } else {
                    /** @var Address $address */
                    $address = Address::create([
                        'longitude' => $request->get('longitude'),
                        'latitude' => $request->get('latitude'),
                        'address' => $data->results[0]->formatted_address,
                        'place_id' => $data->results[0]->place_id
                    ]);
                }
            }

            return response()->json(['address' => $address->address]);

        } catch (Throwable $throwable) {
            Log::debug('Error with find coordinates -'
                . ' longitude: ' . $request->get('longitude')
                . ' latitude: ' . $request->get('latitude'));
            Log::debug('Error message: ' . $throwable->getMessage());
        }

        return response()->json([], 404);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function getAddressesByIdRegion(Request $request): JsonResponse
    {
        try {
            /** @var Region $region */
            $region = Region::find($request->get('region_id'));

            if ($region) {
                $resource = AddressResource::collection($region->addresses);

                return response()->json($resource);
            }

            return response()->json(['error' => 'Wrong region_id.'], 404);
        } catch (Throwable $throwable) {
            Log::debug('Error getAddressesByIdRegion with region_id:' . $region->id ?? null
                . ' Message' . $throwable->getMessage());
        }

        return response()->json(['error' => 'Something was gone wrong.'], 400);
    }
}
